declare module 'redux-react-session' {
  import {Store} from 'redux';
  interface Options {
    refreshOnCheckAuth?: boolean
    redirectPath?: string
    expires?: number
    driver?: 'COOKIES'
    server?: boolean
    validateSession?: (() => Promise<boolean>) | (() => boolean)
  }
  export class sessionService {
    constructor(store: Store, options: any);
    public static initSessionService(store: Store, options?: Options);
    public static saveSession(session: any): Promise<void>;
    public static saveUser(user: any): Promise<void>;
    public static deleteSession(): Promise<void>;
    public static deleteUser(): Promise<void>;
  }
  export function sessionReducer(): any;
}
