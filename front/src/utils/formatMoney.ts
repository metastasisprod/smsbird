const RUBLE = '\u20BD';

export default function formatMoney(money: number | string) {
  return `${money} ${RUBLE}`;
}
