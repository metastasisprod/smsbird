import {FileInput, FormGroup} from '@blueprintjs/core';
import React, {useReducer, useCallback, useEffect} from 'react';
import {uploadFile} from '../api';


type Props = {
  name: string,
  id: string,
  onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void,
  onInputChange: (eventOrPath: string | React.ChangeEvent<any>) => void,
};


function reducer(state: any, event: any) {
  switch (state.status) {
    case 'idle':
    case 'ready':
    case 'error':
      switch (event.type) {
        case 'UPLOAD_START':
          return {...state, status: 'uploading', actions: [{type: 'UPLOAD_EFFECT', payload: event.payload}]};
        case 'UPLOAD_READY':
        case 'UPLOAD_ERROR':
        case 'READY':
        default:
          return state;
      }
    case 'uploading':
      switch (event.type) {
        case 'UPLOAD_START':
          return state;
        case 'UPLOAD_ERROR':
          return {...state, status: 'error', actions: []};
        case 'UPLOAD_READY':
          return {...state, status: 'ready', actions: []};
        case 'READY':
        default:
          return state;
      }
    default:
      return state;
  }
}

function handleUpload(dispatch: any, event: any) {
  dispatch({type: 'UPLOAD_START', payload: {file: event.target.files[0]}});
}

export default function FileUpload(props: Props) {
  const {name, id, onBlur} = props;
  const inputProps = {name, onBlur};
  const [state, dispatch] = useReducer(reducer, {status: 'idle', actions: []});
  const onUpload = useCallback((event: any) => handleUpload(dispatch, event), [dispatch]);
  useEffect(() => {
    state.actions.forEach((action: any) => {
      if (action.type === 'UPLOAD_EFFECT') {
        uploadFile({file: action.payload.file, id})
          .then(data => {
            dispatch({type: 'UPLOAD_READY', user: data});
          })
          .catch(() => dispatch({type: 'UPLOAD_ERROR'}));
      }
    });
  }, [state.actions, dispatch, id]);
  return (
    <FormGroup labelFor={props.name}>
      <FileInput
        id={name}
        inputProps={inputProps}
        onInputChange={onUpload}
        text={statusToString(state.status)}
        placeholder="Данные*"
        buttonText="Загрузить"
      />
    </FormGroup>
  );
}


function statusToString(status: string) {
  switch (status) {
    case 'uploading':
      return 'Загрузка';
    case 'error':
      return 'Ошибка';
    default:
      return 'Данные для шаблона';
  }
}
