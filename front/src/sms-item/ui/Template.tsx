import React from 'react';


interface Props {
  template: {rendered: string[]}
}


export default function Template(props: Props) {
  const {template} = props;
  if (!template) return <p>Загрузите файл с данными, чтобы увидеть примеры сообщений</p>;
  return (
    <>
      {template.rendered.map(r => <p key={r}>{r}</p>)}
    </>
  );
}
