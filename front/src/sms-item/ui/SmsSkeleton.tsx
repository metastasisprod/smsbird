import React from 'react';
import {Button, Classes, FormGroup, H2, TextArea} from '@blueprintjs/core';


export default function Skeleton() {
  return (
    <form>
      <H2 className={Classes.SKELETON}>Sms</H2>
      <FormGroup
        className={Classes.SKELETON}
        helperText="Номера через запятую. Например: 79001002030, 79001002031"
        labelFor="phones"
      >
        <TextArea
          id="phones"
          name="phones"
          placeholder="Мобильные номера*"
          fill
          growVertically
        />
      </FormGroup>
      <FormGroup className={Classes.SKELETON}>
        <TextArea
          name="message"
          placeholder="Сообщение*"
          fill
          growVertically
        />
      </FormGroup>
      <Button
        className={Classes.SKELETON}
        type="submit"
        large
        text="Отправить"
      />
      <Button
        className={Classes.SKELETON}
        large
        text="Рассчитать"
      />
    </form>
  );
}
