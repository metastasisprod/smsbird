import React, {useCallback, useEffect, useState, useReducer} from 'react';
import {RouteChildrenProps} from 'react-router';
import {
  Button,
  Callout,
  FormGroup,
  H2,
  Intent,
  TextArea,
  Dialog
} from '@blueprintjs/core';
import {Formik, FormikHelpers, FormikProps} from 'formik';
import {validatePhones, validateSmsMessage} from '../validators/sms';
import {getSms, saveSms, sendSms, SmsResponse} from '../api';
import FileUpload from './FileUpload';
import formatMoney from '../../utils/formatMoney';
import SmsSkeleton from './SmsSkeleton';
import Template from './Template';


interface Props {
  className?: string,
  smsValues?: any
}

interface FormValues {
  id: string,
  phones: string,
  message: string,
  cost: number,
  count: number
}

type MatchProps = { smsId?: string };
type SmsProps = (
  Props
  & FormikProps<FormValues>
  & RouteChildrenProps<MatchProps>
  );

const errorMsg: { [status: string]: string } = {
  error: 'Что-то пошло не так, попробуйте еще раз позже',
  sendFailed: 'Не удалось отправить смс'
};


const Sms: React.FC<SmsProps> = (props) => {
  const {
    className,
    values,
    smsValues,
    errors,
    status,
    setStatus,
    setValues,
    setSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
  } = props;
  useEffect(() => {
    if (!smsValues) return;
    setValues({
      ...smsValues,
      phones: smsValues.phones.join(',')
    });
  }, [smsValues, setValues]);
  const [state, dispatch] = useReducer(reducr, {opened: false});
  const onShowRenderedSms = useCallback(
    () => dispatch({type: 'SHOW_RENDERED_SMS'}),
    [dispatch]
  );
  const onHideRenderedSms = useCallback(
    () => dispatch({type: 'CLOSE_RENDERED_SMS'}),
    [dispatch]
  );
  const onSaveSms = useCallback(() => {
    setSubmitting(true);
    const nextValues = {
      id: values.id,
      phones: values.phones.replace(/[ \n-)(]/gi, '').split(',').filter(Boolean),
      message: values.message.trim()
    };
    return saveSms(nextValues)
      .then(response => {
        if (response.status !== 'ok') {
          setSubmitting(false);
          setStatus('error');
          return;
        }
        const sms = response.payload;
        setValues({
          id: sms.id,
          phones: sms.phones.join(','),
          message: sms.message,
          cost: sms.cost,
          count: sms.count
        });
        setStatus('saved');
        setSubmitting(false);
      })
      .catch(() => {
        setStatus('error');
        setSubmitting(false);
      });
  }, [setStatus, setValues, setSubmitting, values]);
  const hasError = Boolean(errors.phones || errors.message);
  return (
    <form className={className} onSubmit={handleSubmit}>
      <H2>Sms</H2>
      <FormGroup
        helperText="Номера через запятую. Например: 79001002030, 79001002031"
        labelFor="phones"
      >
        <TextArea
          id="phones"
          name="phones"
          placeholder="Мобильные номера*"
          fill
          growVertically
          value={values.phones}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FormGroup>
      <FileUpload
        name="data"
        id={values.id}
        onInputChange={handleChange}
        onBlur={handleBlur}
      />
      <FormGroup
        helperText={values.cost ? `Стоимость ${formatMoney(values.cost)}` : undefined}
        labelFor="message"
      >
        <TextArea
          name="message"
          placeholder="Сообщение*"
          fill
          growVertically
          value={values.message}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FormGroup>
      {(status === 'error' || status === 'sendFailed') && (
        <Callout intent={Intent.DANGER}>{errorMsg[status]}</Callout>
      )}
      <Button
        large
        text="Сохранить"
        onClick={onSaveSms}
        loading={isSubmitting}
        disabled={isSubmitting || hasError}
      />
      <Button
        large
        text="Пример смс"
        onClick={onShowRenderedSms}
        loading={isSubmitting}
        disabled={isSubmitting}
      />
      <Button
        type="submit"
        large
        text="Отправить"
        loading={isSubmitting}
        disabled={isSubmitting || hasError}
      />
      <Dialog
        title="Пример смс"
        icon="info-sign"
        isOpen={state.opened}
        autoFocus
        canEscapeKeyClose
        canOutsideClickClose
        enforceFocus
        usePortal
        onClose={onHideRenderedSms}
      >
        <Template template={smsValues.template} />
      </Dialog>
    </form>
  );
};


function reducr(state: any, event: any) {
  switch (event.type) {
    case 'SHOW_RENDERED_SMS':
      return {...state, opened: true};
    case 'CLOSE_RENDERED_SMS':
      return {...state, opened: false};
    default:
      return state;
  }
}


const initialValues: FormValues = {
  id: '',
  phones: '',
  message: '',
  cost: 0,
  count: 0
};

function validate(values: FormValues) {
  const phones = validatePhones(values.phones);
  const message = validateSmsMessage(values.message);
  if (phones || message) return {phones, message};
}

function onSubmit({id, phones, message}: FormValues, actions: FormikHelpers<FormValues>) {
  const values = {
    id,
    phones: phones.replace(/[ \n-)(]/gi, '').split(',').filter(Boolean),
    message: message.trim()
  };
  return sendSms(values).then(
    (res) => {
      actions.setSubmitting(false);
      if (res.status !== 'ok') {
        actions.setStatus('sendFailed');
      } else {
        actions.setStatus('sent');
      }
      return res;
    },
    () => {
      actions.setSubmitting(false);
      actions.setStatus('error');
    }
  );
}

type SmsFormState = {
  status: 'loading' | 'done',
  values: SmsResponse | null
}
const SmsForm: React.FC<RouteChildrenProps<MatchProps>> = (props) => {
  const [state, setState] = useState<SmsFormState>({
    status: 'loading',
    values: null
  });
  useEffect(() => {
    // TODO: error case
    if (
      !props.match
      || !props.match.params
      || !props.match.params.smsId
    ) return;
    getSms(props.match.params.smsId).then(response => {
      if (response.status !== 'ok') return;
      const sms = response.payload;
      setState({
        status: 'done',
        values: {
          ...sms,
          phones: sms.phones || initialValues.phones,
          message: sms.message || initialValues.message,
        }
      });
    });
  }, [props.history.location, props.match, setState]);
  const handleSubmit = useCallback((formValues: FormValues, actions: FormikHelpers<FormValues>) => {
    onSubmit(formValues, actions).then(res => {
      if (res && res.status === 'ok') {
        setState({
          status: state.status,
          values: res.payload
        });
      }
    });
  }, [state.status, setState]);
  if (state.status === 'loading') return <SmsSkeleton/>;
  if (state.values && ['new', 'filled'].includes(state.values.status)) {
    return (
      <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={handleSubmit}
      >
        {(formikProps) =>
          <Sms
            {...formikProps}
            {...props}
            smsValues={state.values}
          />
        }
      </Formik>
    );
  }
  if (state.status === 'done' && state.values) {
    return <SmsDetails sms={state.values}/>;
  }
  return <SmsSkeleton/>;
};

export default SmsForm;


function SmsDetails(props: { sms: any }) {
  const {sms} = props;
  return (
    <div>
      <H2>Sms</H2>
      <div>status: {sms.status}</div>
      <div>
        {sms.phones.map((phone: string) => {
          const info = sms.phonesInfo[phone];
          return (
            <span key={info.phone}>
              {info.phone}{info.status ? ` (${info.status})` : ''}{', '}
            </span>
          );
        })}
      </div>
      <div>
        {sms.message}
      </div>
      <div>
        {sms.cost} ({sms.count} шт)
      </div>
    </div>
  );
}
