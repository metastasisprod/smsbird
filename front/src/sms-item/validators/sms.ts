export function validatePhone(_phone: string) {
  return true;
}

export function validatePhones(phones?: string) {
  if (!phones) return 'Укажите хотя бы один номер';
  if (/[^\d \n,\-()]/.test(phones)) return 'Укажите валидные номера';
  const phoneList = phones.replace(/[ \n-]/gi, '').split(',').filter(Boolean);
  if (!phoneList.length) return 'Укажите хотя бы один номер';
  if (!phoneList.every(validatePhone)) return 'Исправьте номера';
  return null;
}

export function validateSmsMessage(message?: string) {
  return message && message.length ? null : 'Укажите сообщение';
}
