import {get, postJson, upload} from '@app/utils';
import {SMS_BACKEND_URL} from '../backend.config';
import {Response} from '../network';


export function getBalance() {
  return get<Response<BalanceResponse>>(`${SMS_BACKEND_URL}/sms/balance`);
}


export function sendSms(payload: {id: string, phones: string[], message: string}) {
  return postJson<Response<SmsResponse>>(
    `${SMS_BACKEND_URL}/sms/${payload.id}/send`,
    payload
  );
}


export function getSms(smsId: string) {
  return get<Response<SmsResponse>>(`${SMS_BACKEND_URL}/sms/${smsId}`);
}


export function saveSms(payload: {id: string, phones: string[], message: string}) {
  return postJson<Response<SmsResponse>>(
    `${SMS_BACKEND_URL}/sms/${payload.id}`,
    payload
  );
}


export function uploadFile({id, file}: {id: string, file: File}) {
  return upload<Template>(`${SMS_BACKEND_URL}/sms/${id}/data`, file);
}


interface BalanceResponse {
  balance: number,
  currency: string
}


export interface SmsResponse {
  id: string,
  status: string,
  phones: string[],
  phonesInfo: Array<{
    phone: string,
    status: string
  }> | null,
  message: string,
  template: Template | null,
  cost: number,
  count: number,
}

export interface Template {
  id: string,
  data: string,
  rendered: string[] | null,
  type: string
}