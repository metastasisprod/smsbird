export type Response<R = NetworkResponse<any>, E = NetworkError<any>> =
  | NetworkResponse<R>
  | NetworkError<E>;

export interface NetworkResponse<T> {
  status: 'ok',
  payload: T
}

export interface NetworkError<E> {
  status: 'error',
  payload: E
}
