import {postJson} from '@app/utils';
import {SMS_BACKEND_URL} from '../backend.config';
import {Response} from '../network';


export function createSms() {
  return postJson<Response<CreateResponse>>(
    `${SMS_BACKEND_URL}/sms/create`
  );
}


export function loadList() {
  return postJson<Response<Sms[]>>(
    `${SMS_BACKEND_URL}/sms/list`
  );
}


interface CreateResponse {
  id: string,
  status: string
}


export interface Sms {
  id: string,
  status: string,
  phones: string[],
  message: string
}
