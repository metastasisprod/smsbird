import React, {useCallback, useEffect, useState} from 'react';
import {Classes, Button, H2, HTMLTable} from '@blueprintjs/core';
import {RouteChildrenProps} from 'react-router';
import {Link} from 'react-router-dom';
import {createSms, loadList, Sms} from '../api';


type ListState = (
  | {status: 'none' | 'loading' | 'error'}
  | {status: 'ready', list: Sms[]}
  );


const List: React.FC<RouteChildrenProps> = (props) => {
  const [creation, setCreation] = useState('none');
  const onCreate = useCallback(() => {
    setCreation('loading');
    createSms()
      .then(response => {
        if (response.status === 'error') {
          setCreation('error');
          return;
        }
        const sms = response.payload;
        setCreation('done');
        props.history.push(`/sms/${sms.id}`);
      })
      .catch(() => setCreation('error'));
  }, [props.history]);
  const [state, setState] = useState<ListState>({status: 'none'});
  useEffect(() => {
    setState({status: 'loading'});
    loadList()
      .then(response => {
        if (response.status === 'error') {
          setState({status: 'error'});
          return;
        }
        setState({status: 'ready', list: response.payload});
      })
      .catch(() => setState({status: 'error'}));
  }, []);
  return (
    <div>
      <H2>List</H2>
      <Button
        text="Создать"
        onClick={onCreate}
        large
        disabled={creation === 'loading'}
      />
      {state.status === 'ready' && (
        <HTMLTable className={Classes.HTML_TABLE_CONDENSED}>
          <tbody>
          {state.list.map(sms => (
            <tr key={sms.id}>
              <td><Link to={{pathname: `/sms/${sms.id}`}}>{sms.id}</Link></td>
              <td>{mapSmsStatus(sms.status)}</td>
            </tr>
          ))}
          </tbody>
        </HTMLTable>
      )}
    </div>
  );
};

export default List;


function mapSmsStatus(status: string) {
  // TODO: Добавить типы статуса из backend sms модели
  switch (status) {
    case 'sent':
      return 'Отправлено';
    case 'delivered':
      return 'Доставлено';
    case 'outdated':
      return 'Устарело';
    case 'error':
      return 'Ошибка при отправке';
    case 'new':
    case 'filled':
    default:
      return 'В процессе';
  }
}
