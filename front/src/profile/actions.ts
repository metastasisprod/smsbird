// @ts-ignore
import {sessionService} from 'redux-react-session';
import {login as apiLogin, logout as apiLogout} from './api';


export const login = (user: any, history: any) => {
  return () => {
    return apiLogin(user)
      .then(response => {
        const saved = sessionService.saveSession({
          token: response.status === 'ok'
        });
        return saved.then(() => response);
      })
      .then(response => sessionService.saveUser(response))
      .then(() => history.push('/'));
  };
};


export const logout = (history: any) => {
  return () => {
    return apiLogout().then(() => {
      sessionService.deleteSession();
      sessionService.deleteUser();
      history.push('/login');
    });
  };
};
