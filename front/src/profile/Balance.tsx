import React, {useEffect, useState} from 'react';
import {Classes, Tooltip} from '@blueprintjs/core';
import {getBalance} from '../sms-item/api';
import formatMoney from '../utils/formatMoney';


export default function Balance() {
  const [balance, setBalance] = useState<number>((null as any));
  useEffect(() => {
    getBalance().then(res => {
      if (res.status === 'ok') {
        setBalance(res.payload.balance);
      }
    });
  }, []);
  if (balance === null) return null;
  return (
    <Tooltip className={Classes.TOOLTIP_INDICATOR} content="Ваш баланс">
      <div>
        {formatMoney(balance)}
      </div>
    </Tooltip>
  );
}
