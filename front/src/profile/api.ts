import {postJson} from '@app/utils';
import {SMS_BACKEND_URL} from '../backend.config';


export function login(params: LoginParams) {
  return postJson<LoginResponse>(
    `${SMS_BACKEND_URL}/login`,
    params
  );
}


export function logout() {
  return postJson<LoginResponse>(`${SMS_BACKEND_URL}/logout`);
}


export const validate = () => {
  return postJson<LoginResponse>(
    `${SMS_BACKEND_URL}/validate`,
  ).then(response => response.status === 'ok');
};


export interface LoginParams {
  phone: string,
  otpCode: string
}

export interface LoginResponse {
  status: string
}
