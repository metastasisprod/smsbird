import React from 'react';
import {useFormik} from 'formik';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {login} from './actions';
import {FormGroup, InputGroup, Button, Intent, H2} from '@blueprintjs/core';


interface Props {
  onLogin: (loginParams: any, history: any) => void,
  history: any
}

const mapDispatch = (dispatch: any) => {
  return {
    onLogin: bindActionCreators(login, dispatch)
  };
};

export default withRouter(connect(null, mapDispatch)(Login));


function Login(props: Props) {
  const formik = useFormik({
    initialValues: {
      phone: '',
      otpCode: ''
    },
    onSubmit: values => {
      props.onLogin(values, props.history);
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <H2>Вход</H2>
      <FormGroup labelFor="phone">
        <InputGroup
          id="phone"
          name="phone"
          type="phone"
          placeholder="Мобильный телефон"
          onChange={formik.handleChange}
          value={formik.values.phone}
        />
      </FormGroup>
      <FormGroup labelFor="otpCode">
        <InputGroup
          id="otpCode"
          name="otpCode"
          type="otpCode"
          placeholder="Код подтверждения"
          onChange={formik.handleChange}
          value={formik.values.otpCode}
        />
      </FormGroup>
      <Button type="submit" loading={formik.isSubmitting} intent={Intent.PRIMARY}>Отправить</Button>
    </form>
  );
}
