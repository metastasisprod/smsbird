import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {
  Navbar,
  NavbarGroup,
  NavbarHeading,
  NavbarDivider,
  Classes,
  Alignment
} from '@blueprintjs/core';
import {connect, Provider} from 'react-redux';
import {combineReducers, createStore, compose, applyMiddleware} from 'redux';
import {sessionService, sessionReducer} from 'redux-react-session';
import thunkMiddleware from 'redux-thunk';
import {Grid, Header, OneColumn, PrivateRoute} from './layout';
import {Link} from './@ui/Link';
import {Balance, Login, validate as validateSession} from './profile';
import {List as SmsList} from './sms-list';
import {Sms as SmsItem} from './sms-item';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import './layout/breakpoints.css';
import './index.css';
import * as serviceWorker from './serviceWorker';


const reducer = combineReducers({
  session: sessionReducer
});

const store = createStore(
  reducer,
  undefined,
  compose(applyMiddleware(thunkMiddleware))
);

// Переписать на свое с использованием cookies и api/validate
sessionService.initSessionService(
  store,
  {driver: 'COOKIES', redirectPath: '/login', validateSession}
);


const App: React.FC = ({checked, authenticated}: any) => {
  if (!checked) return null;
  return (
    <Router>
      <Grid>
        <Header>
          <Navbar>
            {authenticated ? (
              <>
                <NavbarGroup>
                  <NavbarHeading>СМС рассылки</NavbarHeading>
                  <NavbarDivider />
                  <Link
                    to="/"
                    className={Classes.MINIMAL}
                    icon="list-detail-view"
                    text="Рассылки"
                  />
                </NavbarGroup>
                <NavbarGroup align={Alignment.RIGHT}>
                  <Balance />
                </NavbarGroup>
              </>
            ) : (
              <NavbarGroup>
                <NavbarHeading>СМС рассылки</NavbarHeading>
              </NavbarGroup>
            )}
          </Navbar>
        </Header>
        <OneColumn>
          <Switch>
            <PrivateRoute exact path="/" component={SmsList} />
            <PrivateRoute path="/sms/:smsId" component={SmsItem} />
            <Route path="/login" component={Login} />
          </Switch>
        </OneColumn>
      </Grid>
    </Router>
  );
};
const mapStateForApp = ({session}: any) => (
  {
    checked: session.checked,
    authenticated: session.authenticated
  }
);
const MappedApp = connect(mapStateForApp)(App);


ReactDOM.render(
  <Provider store={store}>
    <MappedApp />
  </Provider>,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
