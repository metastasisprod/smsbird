import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';


type Props = any;


function PrivateRoute(props: Props) {
  const {component, exact = false, path, authenticated, location} = props;
  const redirect = {
    pathname: '/login',
    state: {from: location}
  };
  return (
    <Route
      exact={exact}
      path={path}
      render={props => (
        authenticated
          ? React.createElement(component, props)
          : <Redirect to={redirect} />
      )}
    />
  );
}

const mapState = ({session}: any) => (
  {
    authenticated: session.authenticated
  }
);
export default connect(mapState)(PrivateRoute);