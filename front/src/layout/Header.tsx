import React from 'react';
import styles from './common.module.css';

const Header: React.FC = (props) => {
  return (
    <div className={styles.header}>
      {props.children}
    </div>
  );
};

export default Header;
