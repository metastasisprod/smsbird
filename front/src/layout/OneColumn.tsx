import React from 'react';
import styles from './common.module.css';

const OneColumn: React.FC = (props) => {
  return (
    <div className={styles.content}>
      <div className={styles.centered}>
        {props.children}
      </div>
    </div>
  );
};

export default OneColumn;
