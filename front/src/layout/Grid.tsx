import React from 'react';
import styles from './common.module.css';

const Grid: React.FC = (props) => {
  return (
    <div className={styles.grid}>
      {props.children}
    </div>
  );
};

export default Grid;
