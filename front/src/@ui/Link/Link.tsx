import React from 'react';
import {Link} from 'react-router-dom';
import cc from 'classnames';
import {Classes, Icon, IconName, MaybeElement} from '@blueprintjs/core';

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  to: string,
  text?: string,
  disabled?: boolean,
  icon?: IconName | MaybeElement
}

const CustomLink: React.FC<Props> = (props) => {
  const {href, tabIndex = 0, text, children, className, disabled, icon, ...restProps} = props;
  return (
    <Link
      {...restProps}
      className={cc(Classes.BUTTON, className)}
      rel="noopener noreferrer"
      role="button"
      href={disabled ? undefined : href}
      tabIndex={disabled ? -1 : tabIndex}
    >
      <Icon key="icon-left" icon={icon} />
      <span className={Classes.BUTTON_TEXT}>
        {text || children}
      </span>
    </Link>
  );
};

export default CustomLink;
