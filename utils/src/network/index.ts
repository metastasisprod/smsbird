import fetch from 'cross-fetch';
import qs from 'qs';


export function postJson<Result = any, Body = any>(
  url: string,
  body?: Body
): Promise<Result> {
  const headers = {
    'Content-Type': 'application/json'
  };
  return fetch(url, {
    // TODO: think about it
    credentials: 'include',
    method: 'post',
    headers,
    body: JSON.stringify(body),
  }).then(res => res.json());
}


export function get<Result = any, Body = {[key: string]: any}>(
  url: string,
  body?: Body
): Promise<Result> {
  const urlWithParams = `${url}?${qs.stringify(body)}`;
  const headers = {
    'Content-Type': 'application/json'
  };
  return fetch(urlWithParams, {
    credentials: 'include',
    method: 'get',
    headers
  }).then(res => res.json());
}


export function upload<Result extends any = any, Body extends File = File>(
  url: string,
  file: Body
): Promise<Result> {
  const form = new FormData();
  form.append('file', file, file.name);
  return fetch(url, {
    credentials: 'include',
    method: 'post',
    body: form
  }).then((res: Response) => res.json());
}