import config from './config';
import mongoose from 'mongoose';
import log from 'roarr';
import {serializeError} from 'serialize-error';


export default () => {
  mongoose.connect(
    `mongodb+srv://${config.mongodb.user}:${config.mongodb.pass}@${config.mongodb.host}/${config.mongodb.db}`,
    {useNewUrlParser: true}
  );
  mongoose.connection.once('open', () => {
    log.info('Mongoose connection established');
  });
  mongoose.connection.once('close', () => {
    log.info('Mongoose connection closed');
  });
  mongoose.connection.on('error', (err) => {
    log.error({error: serializeError(err)}, err.toString());
  });
};
