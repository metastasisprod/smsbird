import express from 'express';
import logger from 'morgan';
import cors from 'cors';
// @ts-ignore
import expressSession from 'express-session';
import passport from 'passport';
import smsRouter from './sms/router';
import {ApplicationError, UserFacingError} from './error/base';
import {SmscError, ValidationError} from './error/smsc';
import {NotFoundError} from './error/request';
import log from 'roarr';
import {serializeError} from 'serialize-error';
import config from './config';
import {ensureLoggedIn, initPassport} from './auth';


initPassport();
const app = express();
const allowedOrigins = ['http://localhost:3000', 'http://localhost:3001'];
app.use(cors({
  credentials: true,
  origin: (origin, done) => {
    // allow requests with no origin
    // (like mobile apps or curl requests)
    if (!origin) return done(null, true);
    return (
      allowedOrigins.indexOf(origin) === -1
        ? done(new Error('CORS error'), false)
        : done(null, true)
    );
  }
}));

app.use(logger('dev'));
app.use(express.json());
app.use(expressSession({
  secret: config.session,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());


app.post(
  '/api/v1/login',
  passport.authenticate('local'),
  (req, res) => {
    res.json({status: 'ok'});
  }
);
app.post(
  '/api/v1/validate',
  ensureLoggedIn(),
  (req, res) => {
    res.json({status: 'ok'});
  }
);
app.post('/api/v1/logout', function (req, res) {
  req.logout();
  res.redirect('/');
});

app.use('/api/v1/sms', smsRouter);
app.use((error: ApplicationError, req: any, res: any, next: any) => {
  if (
    error instanceof UserFacingError
    || error instanceof SmscError
    || error instanceof NotFoundError
    || error instanceof ValidationError
  ) {
    log.error({error: serializeError(error)}, error.toString());
    res.status(error.statusCode).json(error.toJson());
  } else {
    log.error({error: serializeError(error)}, error.toString());
    res.sendStatus(500).json({status: 'error'});
  }
});


export default app;
