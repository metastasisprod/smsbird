import app from './app';
import smscApi from './sms/api/smsc';
import config from './config';
import dbConnect from './db-connection';
import mongoose from "mongoose";
import log from 'roarr';
import Signals = NodeJS.Signals;
import {serializeError} from 'serialize-error';


dbConnect();
smscApi.configure(config.smsc);


mongoose.connection.once('open', () => {
  log.info('Server process starting');

  app.listen(config.express.port, config.express.ip, (error: any) => {
    if (error) {
      log.error({error: serializeError(error)}, error.toString());
      process.exit(10);
    }
    log.info(
      `Express is listening on http://${config.express.ip}:${config.express.port}`
    );
  });

  (['SIGINT', 'SIGTERM', 'SIGQUIT'] as Signals[]).forEach(sig => {
    process.on(sig, () => {
      mongoose.disconnect();
    })
  });
});
