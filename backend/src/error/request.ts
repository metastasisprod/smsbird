import {UserFacingError} from './base';

export class BadRequest extends UserFacingError {
  constructor(message: string, options?: {params: any}) {
    super(message);
    if (!options) return;
    if (options.params) this.params = options.params;
  }

  get statusCode() {
    return 400;
  }
}

export class Unauthorized extends UserFacingError {
  constructor(message: string, options?: {params: any}) {
    super(message);
    if (!options) return;
    if (options.params) this.params = options.params;
  }

  get statusCode() {
    return 401;
  }
}

export class InternalError extends UserFacingError {
  get statusCode() {
    return 500;
  }
}

export class NotFoundError extends UserFacingError {
  constructor(message: string = 'Not found') {
    super(message);
  }

  get statusCode() {
    return 404;
  }
}
