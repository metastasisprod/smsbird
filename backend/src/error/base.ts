export class ApplicationError extends Error {
  params?: any;

  constructor(message: string, options?: {params: any}) {
    super(message);
    if (!options) return;
    this.params = options.params;
  }

  get name() {
    return this.constructor.name;
  }

  get statusCode() {
    return 500;
  }

  toJson() {
    return {status: 'error'};
  }
}

export class UserFacingError extends ApplicationError {}
