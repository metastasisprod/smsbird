import {BadRequest} from './request';
import {UserFacingError} from './base';

export class SmscError extends BadRequest {
  params: any;
  errorCode: any;

  constructor(message: string, options?: {params: any, errorCode: number}) {
    super(message);
    if (!options) return;
    this.params = options.params;
    this.errorCode = options.errorCode;
  }

  toJson() {
    return {
      status: 'error',
      errorCode: this.errorCode,
      message: this.message
    };
  }
}

export class SmsNotFoundError extends UserFacingError {
  constructor(options: {params: any}) {
    super('Sms not found', options);
  }
}

export class ValidationError extends UserFacingError {
  get statusCode() {
    return 403;
  }
}
