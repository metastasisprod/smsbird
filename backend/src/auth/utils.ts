import {NextFunction, Request, Response} from 'express';
import {Unauthorized} from '../error/request';


export function ensureLoggedIn() {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.isAuthenticated()) {
      throw new Unauthorized('Unauthorized', req);
    }
    next();
  }
}
