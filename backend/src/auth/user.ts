import mongoose, {Schema, Document} from 'mongoose';
import uuidv4 from 'uuid/v4';


export interface User extends Document {
  _id: string,
  phone: string,
  createdAt: string,
  updatedAt: string
}


const User = new Schema({
  _id: {type: String, default: uuidv4},
  phone: {type: String, default: null, unique: true},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date},
});


User.pre('save', function (next) {
  // @ts-ignore
  this.updatedAt = Date.now();
  next();
});


export default mongoose.model<User>('User', User);
