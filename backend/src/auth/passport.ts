import passport from 'passport';
// @ts-ignore
import {Strategy as LocalStrategy} from 'passport-local';
import User, {User as UserModel} from './user';


type StrategyCb = (err: null | Error, user?: UserModel | false) => void;
type Identifier = UserModel['phone'];


export default function init() {
  const options = {usernameField: 'phone', passwordField: 'otpCode'};
  passport.use(new LocalStrategy(options, authCallback));
  passport.serializeUser(serialize);
  passport.deserializeUser(deserialize);
}


function authCallback(
  phone: string,
  otpCode: string,
  done: StrategyCb
) {
  User.findOne({phone}).then((user: null | UserModel) => {
    if (!user || otpCode !== '1111') return done(null, false);
    return done(null, user);
  }, done);
}

function serialize(
  user: UserModel,
  done: (err: null | Error, user: Identifier | false) => void
) {
  done(null, user.phone);
}

function deserialize(phone: string, done: StrategyCb) {
  User.findOne({phone}).then((user: null | UserModel) => {
    if (!user) return done(null, false);
    return done(null, user);
  }, done);
}
