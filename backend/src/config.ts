interface Config {
  express: {ip: string, port: number};
  mongodb: {user: string, pass: string, db: string, host: string, port: number};
  smsc: {login: string, password: string};
  session: string
}

const express = {
  port: parseInt(process.env.EXPRESS_PORT || '', 10) || 3001,
  ip: '127.0.0.1'
};

const mongodb = {
  db: process.env.MONGODB_DATABASE || '',
  port: parseInt(process.env.MONGODB_PORT || '', 10) || 27017,
  host: process.env.MONGODB_HOST || 'localhost',
  user: process.env.MONGODB_USER || 'admin',
  pass: process.env.MONGODB_PASS || ''
};

const smsc = {
  login: process.env.SMSC_LOGIN || '',
  password: process.env.SMSC_PASSWORD || ''
};

const session = process.env.SESSION_SECRET || 'keyboard cat';

const config: Config = {express, mongodb, smsc, session};

export default config;
