import mongoose, {Schema, Document} from 'mongoose';
import uuidv4 from 'uuid/v4';

export enum SmsStatus {
  New = 'new',
  Filled = 'filled',
  Sent = 'sent',
  Delivered = 'delivered',
  Outdated = 'outdated',
  Error = 'error',
}

export enum SmscStatus {
  NotFound = 'not found',
  Stopped = 'stopped',
  AwaitSending = 'await sending',
  SentToOperator = 'sent to operator',
  Delivered = 'delivered',
  Read = 'read',
  Outdated = 'outdated',
  LinkClicked = 'link clicked',
  DeliveryError = 'delivery error',
  PhoneWrong = 'phone wrong',
  PhoneUnavailable = 'phone unavailable',
  BalanceError = 'balance error',
}

export interface PhoneInfo {
  status: SmscStatus,
  phone: string,
  cost: number,
  mccmnc: string
  error?: string
}

export interface Sms extends Document {
  _id: string,
  origin: '' | 'smsc' | 'demo',
  status: SmsStatus,
  phones: string[],
  phonesInfo: {
    [phone: string]: PhoneInfo
  } | null,
  message: string,
  cost: number,
  count: number | void,
  template: Template | null,
  createdAt: Date,
  updatedAt: Date,
}

export interface Template {
  id: string,
  data: string,
  type: string | null,
  rendered: string[] | null
}


const Sms = new Schema({
  _id: {type: String, default: uuidv4},
  origin: {type: String, default: ''},
  status: {type: SmsStatus, default: SmsStatus.New},
  phones: {type: [String], default: []},
  phonesInfo: {type: Object, default: null},
  message: {type: String, default: ''},
  cost: {type: Number, default: 0},
  count: {type: Number, default: 0},
  template: {type: Object, default: null},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date},
});

Sms.pre('save', function (next) {
  // @ts-ignore
  this.updatedAt = Date.now();
  next();
});

export default mongoose.model<Sms>('Sms', Sms);
