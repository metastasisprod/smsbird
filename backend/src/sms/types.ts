export interface BalanceResponse {
  balance: number,
  currency: 'RUR'
}


export interface SmsCostParams {
  id: string,
  phones: string[],
  message: string
}
export interface SmsCostResponse {
  cost: number,
  count: number
}


type PhonesInfo = {
  phone: string,
  mccmnc: string,
  cost: number,
  status: any, // Почему-то отсутствует
  error?: string
};
export interface SmsSendParams {
  id: string,
  phones: string[],
  message: string
}
export interface SmsSendResponse {
  id: string,
  phones: string[],
  phonesInfo: {[phone: string]: PhonesInfo},
  cost: number,
  count: number
}


export interface SmsApi {
  origin: 'smsc' | 'demo',
  getBalance: () => Promise<void | BalanceResponse>,
  getSmsCost: (params: SmsCostParams) => Promise<void | SmsCostResponse>,
  sendSms: (params: SmsSendParams) => Promise<void | SmsSendResponse>
}
