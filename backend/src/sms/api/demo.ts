import {SmsApi, SmsCostParams, SmsSendParams} from '../types';

class DemoSmsApi implements SmsApi {
  origin: 'demo' = 'demo';
  balance = 42;

  getBalance() {
    return Promise.resolve({
      balance: this.balance,
      currency: 'RUR' as const
    });
  }

  sendSms(params: SmsSendParams) {
    const cost = calcCost(params.message);
    if (cost > this.balance) return Promise.reject({error: 'Not enough money'});
    return Promise.resolve({
      id: params.id,
      phones: params.phones,
      phonesInfo: params.phones.reduce((acc: any, phone) => {
        acc[phone] = {
          phone,
          mccmnc: '',
          cost,
          status: 'sent',
        };
        return acc;
      }, {}),
      cost,
      count: 1
    });
  }

  getSmsCost(params: SmsCostParams) {
    return Promise.resolve({cost: calcCost(params.message), count: 1});
  }
}

export default new DemoSmsApi();


function calcCost(message: string) {
  if (!message) return 0;
  if (message.length < 100) return 1;
  return Math.floor(message.length / 100);
}
