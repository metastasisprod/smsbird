// SMSC.RU API (smsc.ru) версия 1.1 (03.07.2019)
// https://smsc.ru/api/code/libraries/http_smtp/nodejs/#menu
import {get} from '@app/utils';
import {SmscError} from '../../error/smsc';
import {SmscStatus} from '../sms';
import {
  SmsCostParams,
  SmsCostResponse as TSmsCostResponse,
  SmsSendParams,
  SmsSendResponse
} from '../types';
import log from 'roarr';


export enum Fmt {
  JSON = 3
}

export enum SmsCostType {
  PRICE = 1,
  SEND_AND_PRICE = 3
}

export interface ApiOptions {
  host?: string,
  login: string,
  password: string,
  ssl?: boolean,
  fmt?: Fmt,
  charset?: 'windows-1251' | 'utf-8' | 'koi8-r'
}

interface Params<T extends {}> {
  endpoint: string,
  data?: T
}

type SmsCostPayload = (
  | { id?: string, type: 'default', cost: 1, phones: string[] | string, mes: string }
  | { id?: string, type: 'list', cost: 1, list: string[] }
  );

type SmsCommon = {
  id?: number | string,
  op?: 1,
  cost?: 0 | 1 | 2 | 3,
  sender?: string,
  time?: any,
  freq?: number,
  flash?: 0 | 1,
  bin?: 0 | 1 | 2,
  push?: 0 | 1,
};

type SmsPayload = (
  | ({ type: 'default', phones: string[] | string, mes: string } & SmsCommon)
  | ({ type: 'list', list: string } & SmsCommon)
  );

type SmsPayloadRaw = Omit<SmsPayload, 'list'> & { list: [string, string][] };

type StatusPayloadRaw = { id: string[], all?: 0 | 1 | 2 };
type StatusPayload = { id: string, all: 0 | 1 | 2 };

// export enum PhoneTypes {
//   string = 1,
//   number = 2
// }

export interface RequestError {
  error: string,
  errorCode: 100
}


export interface BalanceResponse {
  balance: number,
  currency: 'RUR'
}

interface SmsCostResponse {
  cost: number,
  cnt: number
}

interface SmsResponse {
  id: string,
  cost: number,
  cnt: number,
  balance: number,
  phones: Array<{
    phone: string,
    mccmnc: string,
    cost: number,
    status: SmscStatus, // TODO: почему не присылается?
    error?: string
  }>
}

interface StatusResponse {
  status: string,
  // last_date: string,
  last_timestamp: number,
  err: number,
  // send_date: "<send_date>",
  send_timestamp: number,
  phone: string,
  country: string,
  operator: string,
  operator_orig: string,
  region: string,
  cost: number,
  sender: string,
  status_name: string,
  message: string,
  comment: string,
  mccmnc: string,
  type: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 10
}

interface StatusResponseMap {
  status: string,
  lastTimestamp: number,
  err: number,
  sendTimestamp: number,
  phone: string,
  country: string,
  operator: string,
  operatorOrig: string,
  region: string,
  cost: number,
  sender: string,
  statusName: string,
  message: string,
  comment: string,
  mccmnc: string,
  type: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 10
}


class Api {
  origin: 'smsc' = 'smsc';
  private options = {
    ssl: false,
    fmt: Fmt.JSON,
    host: 'smsc.ru',
    charset: 'utf-8',
    login: '',
    password: ''
  };

  constructor(options?: ApiOptions) {
    this.options = {
      ...this.options,
      ...options
    };
  }

  private get_host = (www?: string) => {
    if (!www) www = '';
    return (this.options.ssl ? 'https://' : 'http://') + www + this.options.host + '/sys/';
  };

  // private convert_data = (data: any, notConvert?: any) => {
  //   const payload = {...data};
  //   if (data.fmt) delete payload.fmt;
  //   if (data.msg) {
  //     payload.mes = data.msg;
  //     delete payload.msg;
  //   }
  //   if (data.message) {
  //     payload.mes = data.message;
  //     delete payload.message;
  //   }
  //   if (data.phone && !isInArr(notConvert, 'phone')) {
  //     payload.phones = data.phone;
  //     delete payload.phone;
  //   }
  //   if (data.number) {
  //     payload.phones = data.number;
  //     delete payload.number;
  //   }
  //   if (data.list) {
  //     let list = '';
  //     for (let i in data.list) {
  //       list += i + ':' + data.list[i] + '\n';
  //     }
  //     payload.list = list;
  //     delete payload.mes;
  //   }
  //   if (data.phones && !(typeof data.phones in PhoneTypes)) payload.phones =
  // data.phones.join(','); };

  private getRequest = <T, P>(prs: Params<P>) => {
    const url = this.get_host('') + prs.endpoint;
    const {fmt, login, password: psw, charset} = this.options;
    const body = {
      ...prs.data,
      fmt,
      login,
      psw,
      charset
    };
    log.info(logSecrets(body), `SMSC API REQUEST; ${url}`);
    return get(url, body).then<T>((data: any) => {
      const {error, error_code: errorCode} = data;
      log.info(data, `SMSC API RESPONSE; ${url}`);
      if (error) return Promise.reject({error, errorCode});
      else return data;
    });
  };

  // Конфигурирование
  configure = (prs: ApiOptions) => {
    this.options.ssl = Boolean(prs.ssl);
    this.options.login = prs.login;
    this.options.password = prs.password;
    if (prs.charset) this.options.charset = prs.charset;
  };

  // Отправка сообщения любого типа (data — объект, включающий параметры
  // отправки. Подробнее смотрите в документации к API)
  // send = (type: any, data: any, clb: any) => {
  //   if (typeof data !== 'object') data = {};
  //   const opts = {
  //     type,
  //     endpoint: 'send.php',
  //     data
  //   };
  //   this.read_url(opts, clb);
  // };

  // Отправка простого SMS сообщения
  sendSms = (data: SmsSendParams): Promise<void | SmsSendResponse> => {
    const payload: SmsPayload = {
      phones: data.phones.join(','),
      mes: encodeURIComponent(data.message),
      type: 'default',
      op: 1 as 1,
      cost: SmsCostType.PRICE
    };
    // payload.list = payload.list.reduce(makeMessageList, '').trim();
    delete payload.type;
    return this.getRequest<SmsResponse, SmsPayload>({
      endpoint: 'send.php',
      data: payload
    })
      .then(payload => ({
        ...payload,
        cost: parseFloat(payload.cost as any),
        count: Number(payload.cnt),
        // smsc api реплейсит первую 8 на 7 в телефонах
        phones: payload.phones.map(item => item.phone),
        phonesInfo: payload.phones.reduce((acc: SmsSendResponse['phonesInfo'], item) => {
          acc[item.phone] = {
            ...item,
            cost: parseFloat(payload.cost as any)
          };
          return acc;
        }, {})
      }))
      .catch(error => makeError(error, data));
  };

  // Получение статуса сообщения
  getStatus = (data: StatusPayloadRaw) => {
    const payload = {
      id: data.id.length === 1 ? `${data.id},` : data.id.join(','),
      all: data.all || 2 as 2
    };
    return this.getRequest<StatusResponse, StatusPayload>({
      endpoint: 'status.php',
      data: payload
    })
      .then((payload) => ({
        status: payload.status,
        lastTimestamp: payload.last_timestamp,
        err: payload.err,
        sendTimestamp: payload.send_timestamp,
        phone: payload.phone,
        country: payload.country,
        operator: payload.operator,
        operatorOrig: payload.operator_orig,
        region: payload.region,
        cost: payload.cost,
        sender: payload.sender,
        statusName: payload.status_name,
        message: payload.message,
        comment: payload.comment,
        mccmnc: payload.mccmnc,
        type: payload.type
      } as StatusResponseMap))
      .catch(error => makeError(error, data));
  };

  // Получение баланса
  getBalance = () => {
    const data = {cur: 1 as 1};
    return this.getRequest<BalanceResponse, typeof data>({
      endpoint: 'balance.php',
      data
    })
      .then(payload => ({
        ...payload,
        balance: parseFloat(payload.balance as any)
      }))
      .catch(error => makeError(error, data));
  };

  // Получение стоимости сообщения
  getSmsCost = (data: SmsCostParams): Promise<void | TSmsCostResponse> => {
    const payload: SmsCostPayload = {
      id: data.id,
      phones: data.phones.join(','),
      mes: encodeURIComponent(data.message),
      type: 'default',
      cost: SmsCostType.PRICE
    };
    // payload.list = payload.list.reduce(makeMessageList, '').trim();
    delete payload.type;
    return this.getRequest<SmsCostResponse, Omit<SmsCostPayload, 'type'>>({
      endpoint: 'send.php',
      data: payload
    })
      .then(payload => ({
        ...payload,
        cost: parseFloat(payload.cost as any),
        count: Number(payload.cnt)
      }))
      .catch(error => makeError(error, payload));
  };

  // Запрос к API
  // raw = (endpoint: any, data: any, clb: any) => {
  //   this.read_url({endpoint: endpoint, data: data}, clb);
  // };

  // Тестирование подключения и данных авторизации
  test = this.getBalance;
}

export default new Api();


function makeMessageList(acc: string, item: [string, string]) {
  // 79990001122:msg1\n799900033344:msg2
  const [phoneRaw, messageRaw] = item;
  const phone = phoneRaw.trim();
  const message = messageRaw.trim();
  if (!phone || !message) return acc;
  return `${acc}\n${phone}:${message}`
}


function makeError(error: any, params: any) {
  if (error.errorCode) {
    throw new SmscError(error.error, {
      params,
      errorCode: error.errorCode
    });
  }
  throw new SmscError('General error', {params, errorCode: 1});
}


function logSecrets(data: any) {
  return {
    ...data,
    login: '*********',
    psw: '*********',
  }
}
