export function validateSmsId(params: {[key: string]: any}) {
  if (!params.id) return 'Bad params';
  return null;
}

export function validateSms(params: {[key: string]: any}) {
  if (!params.phones || !params.message) {
    return 'Bad params';
  }
  return null;
}