import express, {Response} from 'express';
import Busboy from 'busboy';
import smscApi from './api/smsc';
import demoApi from './api/demo';
import {BadRequest} from '../error/request';
import SmsController from './controller';
import {validateSmsId, validateSms} from './validators';
import {ensureLoggedIn} from '../auth';


const router = express.Router();
const smsApi = demoApi;


router.use(ensureLoggedIn());


router.get('/balance', (req, res, next) => {
  const ctrl = new SmsController();
  ctrl.getBalance(smsApi).then(payload => toResponse(res, payload)).catch(next);
});


router.post('/create', (req, res, next) => {
  const ctrl = new SmsController();
  ctrl.createSms(smsApi.origin)
    .then(payload => toResponse(res, payload))
    .catch(next);
});


// TODO: get
router.post('/list', (req, res, next) => {
  const ctrl = new SmsController();
  ctrl.find(smsApi.origin)
    .then(payload => toResponse(res, payload))
    .catch(next);
});


router.get('/:id', (req, res, next) => {
  const paramsError = validateSmsId(req.params);
  if (paramsError) {
    next(new BadRequest(paramsError, {params: req.params}));
    return;
  }
  const ctrl = new SmsController();
  ctrl.findById(req.params.id, smsApi.origin)
    .then(payload => toResponse(res, payload))
    .catch(next);
});


router.post('/:id', (req, res, next) => {
  const paramsError = validateSmsId(req.params);
  if (paramsError) {
    next(new BadRequest(paramsError, req));
    return;
  }
  const smsError = validateSms(req.body);
  if (smsError) {
    next(new BadRequest(smsError, req));
    return;
  }
  const ctrl = new SmsController();
  ctrl.saveSms(req, smsApi)
    .then(payload => toResponse(res, payload))
    .catch(next);
});


router.post('/:id/send', (req, res, next) => {
  const paramsError = validateSmsId(req.params);
  if (paramsError) {
    next(new BadRequest(paramsError, req));
    return;
  }
  const smsError = validateSms(req.body);
  if (smsError) {
    next(new BadRequest(smsError, req));
    return;
  }
  const ctrl = new SmsController();
  // TODO: обновлять из send данные, если есть?
  ctrl.sendSms(req, smsApi)
    .then(payload => toResponse(res, payload))
    .catch(next);
});


router.post('/:id/data', (req, res, next) => {
  try {
    const template = {data: '', type: null};
    const ctrl = new SmsController();
    const busboy = new Busboy({
      headers: req.headers,
      defCharset: 'utf8',
      limits: {fieldNameSize: 255, files: 1, fileSize: 1024}
    });
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      if (mimetype !== 'application/json') throw new BadRequest(
        'Invalid content type');
      // @ts-ignore
      template.type = mimetype;
      file.on(
        'data',
        (data) => (
          template.data += data
        )
      );
    });
    busboy.on('finish', () => {
      ctrl.saveTemplate(req, smsApi, template)
        .then(payload => toResponse(res, payload))
        .catch(next);
    });
    req.pipe(busboy);
  } catch (e) {
    next(e);
  }
});


export default router;


function toResponse<T>(res: Response, payload: T) {
  res.json({status: 'ok', payload});
}