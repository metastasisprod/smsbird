import {SmsApi} from "./types";
import SmsModel, {SmsStatus} from './sms';
import {SmsNotFoundError} from "../error/smsc";
import {Request} from 'express';
import handlebars from 'handlebars';
import uuidv4 from "uuid/v4";


export default class Controller {
  getBalance(smsApi: SmsApi) {
    return smsApi.getBalance();
  }

  createSms(origin: SmsApi['origin']) {
    const sms = new SmsModel();
    sms.origin = origin;
    return sms.save()
      .then(() => {
        return {
          id: sms._id,
          status: sms.status,
          phones: sms.phones,
          phonesInfo: sms.phonesInfo,
          message: encodeURIComponent(sms.message),
        };
      })
  }

  find(origin: SmsApi['origin']) {
    return SmsModel.find({status: {$ne: 'new'}, origin})
      .sort({createdAt: 'desc'})
      .limit(20)
      .then(list => list.map(sms => ({
        id: sms._id,
        status: sms.status,
        phones: sms.phones,
        phonesInfo: sms.phonesInfo,
        message: decodeURIComponent(sms.message),
      })))
  }

  findById(id: string, origin: SmsApi['origin']) {
    return SmsModel.findOne({_id: id, origin}).then(sms => {
      if (!sms) throw new SmsNotFoundError({params: {id}});
      return {
        id: sms._id,
        status: sms.status,
        phones: sms.phones,
        phonesInfo: sms.phonesInfo,
        message: decodeURIComponent(sms.message),
        cost: sms.cost,
        count: sms.count,
        template: sms.template,
      };
    })
  }

  saveSms(req: Request, smsApi: SmsApi) {
    return this.getExistentSms(req, smsApi.origin)
      .then(sms => {
        sms.phones = req.body.phones;
        sms.message = encodeURIComponent(req.body.message);
        sms.status = SmsStatus.Filled;
        // TODO: normalize?
        return smsApi.getSmsCost(req.body).then(response => {
          if (response) {
            sms.cost = response.cost;
            sms.count = response.count;
          }
          return sms;
        });
      })
      .then(sms => sms.save())
      .then(savedSms => ({
        id: savedSms._id,
        status: savedSms.status,
        phones: savedSms.phones,
        phonesInfo: savedSms.phonesInfo,
        message: decodeURIComponent(savedSms.message),
        cost: savedSms.cost,
        count: savedSms.count,
        template: savedSms.template,
      }));
  }

  sendSms(req: Request, smsApi: SmsApi) {
    return this.getExistentSms(req, smsApi.origin)
      .then(sms => {
        return smsApi.sendSms({
          id: sms._id,
          phones: sms.phones,
          message: sms.message
        }).then(updatedSms => {
          sms.status = SmsStatus.Sent;
          if (updatedSms) {
            sms.count = updatedSms.count;
            sms.cost = updatedSms.cost;
            sms.phonesInfo = updatedSms.phonesInfo;
          }
          return sms.save();
        }).catch(err => {
          sms.status = SmsStatus.Error;
          sms.save().finally(() => {
            throw err
          });
        })
      })
      .then(savedSms => {
        if (!savedSms) throw new SmsNotFoundError(req);
        return {
          id: savedSms._id,
          status: savedSms.status,
          phones: savedSms.phones,
          phonesInfo: savedSms.phonesInfo,
          message: decodeURIComponent(savedSms.message),
          cost: savedSms.cost,
          count: savedSms.count,
          template: savedSms.template,
        };
      });
  }

  saveTemplate(req: Request, smsApi: SmsApi, rawTemplate: { type: string | null, data: string }) {
    if (rawTemplate.type !== 'application/json') throw new SmsNotFoundError(req);
    const template = {
      ...rawTemplate,
      rendered: null,
      id: uuidv4()
    };
    return this.getExistentSms(req, smsApi.origin)
      .then(sms => {
        const renderTemplate = handlebars.compile(decodeURIComponent(sms.message));
        const allData: Array<{[field: string]: any}> = JSON.parse(template.data);
        sms.template = {
          ...template,
          rendered: allData.map(ctx => renderTemplate(ctx))
        };
        return sms.save();
      }).then(savedSms => {
        if (!savedSms || !savedSms.template) throw new SmsNotFoundError(req);
        return {
          id: savedSms._id,
          status: savedSms.status,
          phones: savedSms.phones,
          phonesInfo: savedSms.phonesInfo,
          message: decodeURIComponent(savedSms.message),
          cost: savedSms.cost,
          count: savedSms.count,
          template: savedSms.template,
        };
      });
  }

  private getExistentSms(req: Request, origin: SmsApi['origin']) {
    return SmsModel.findOne({_id: req.params.id, origin})
      .then(sms => {
        if (!sms || (sms.status !== SmsStatus.New && sms.status !== SmsStatus.Filled)) {
          throw new SmsNotFoundError({params: req.params});
        }
        return sms;
      });
  }
}
